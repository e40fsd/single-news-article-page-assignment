const apiKey = '035c9bcae77c4a72b46d1f71044fcbed';
const apiUrl = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${apiKey}`;

async function getNews() {
    try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        return data.articles;
    } catch (error) {
        console.error('Error fetching news: ', error);
        return [];
    }
}
async function displayNews() {
    const newsContainer = document.getElementById('news-container');
    const articles = await getNews();
    
    articles.forEach(article => {
        const card = document.createElement('div');
        card.classList.add('card');

        const title = document.createElement('h2');
        title.textContent = article.title;

        const description = document.createElement('p');
        description.textContent = article.description;

        const source = document.createElement('p');
        source.innerHTML = `Source: <a href="${article.url}" target="_blank">${article.source.name}</a>`;

        card.appendChild(title);
        card.appendChild(description);
        card.appendChild(source);

        newsContainer.appendChild(card);
    });
}

displayNews();